CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Gin Permissions provides permission settings for the Gin Admin theme

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/gin_permissions

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/gin_permissions


REQUIREMENTS
------------

* Gin Admin (https://www.drupal.org/project/gin):
  A better Content Editor Experience on top of Claro.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


MAINTAINERS
-----------

Current maintainers:
 * Mario Thiele (dermario) - https://www.drupal.org/u/dermario
 * Sascha Eggenberger (saschaeggi) - https://www.drupal.org/u/saschaeggi
